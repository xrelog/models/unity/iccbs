# Intergalactic Chaos Community BS (ICCBS)

This Unity 2020.3.22f1 project is purely for designing and preparing the ICCBS model for [thirdroom.io](https://thirdroom.io/) export. The model is taken from [Kitbash 3D](https://kitbash3d.com) named [Mission to Minerva](https://kitbash3d.com/products/mission-to-minerva). The used shaders will be the GLTF/BRGraph imported via [UnityGLTF](https://github.com/matrix-org/UnityGLTF.git?path=/UnityGLTF/Assets/UnityGLTF#export-files) using the Unity Universal Rendering Pipeline (URP). The [Third Room Exporter](https://github.com/matrix-org/thirdroom-unity-exporter.git) alows you to generate a .glb or a .gltf (with external textures).


## Visuals
![Image of a model from the Mission to Minerva kitbash](https://matrix-client.matrix.org/_matrix/media/r0/download/thirdroom.io/SRagOfdgeESGNiIuQbXAUoYq)*Image by P.T. Fischer*

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

### Mission to Minerva
Designed by Fabio Fernandes \
Modeled by JD Havenga \
Vehicles Modeled by Brandon Acree \
Texturing by Alexey HrDesign \
Cover by William Bang 

## License
The [Mission to Minerva](https://kitbash3d.com/products/mission-to-minerva) kitbash is licensed under the individual license (a.k.a Freelance License) from [Kitbash 3D](https://kitbash3d.com).

Freelance License \
If you are a freelance artist for hire, or a hobbyist, or a student, and not staffed at a studio, this license is for you. Studio projects of any kind are not covered under this license nor can kits with this license be stored on a company server.

For more information on the licensing visit [https://kitbash3d.com/pages/licenses](https://kitbash3d.com/pages/licenses)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
